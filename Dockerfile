FROM maven:3.6.3-jdk-8

ENV ANT_VERSION 1.10.8
ENV ANT_HOME /etc/ant-${ANT_VERSION}

RUN cd /tmp
RUN wget https://mirrors.estointernet.in/apache//ant/binaries/apache-ant-${ANT_VERSION}-bin.tar.gz
RUN mkdir ant-${ANT_VERSION}
RUN tar -zxvf apache-ant-${ANT_VERSION}-bin.tar.gz --directory ant-${ANT_VERSION} --strip-components=1
RUN mv ant-${ANT_VERSION} ${ANT_HOME}
ENV PATH ${PATH}:${ANT_HOME}/bin

RUN rm apache-ant-${ANT_VERSION}-bin.tar.gz
RUN unset ANT_VERSION
